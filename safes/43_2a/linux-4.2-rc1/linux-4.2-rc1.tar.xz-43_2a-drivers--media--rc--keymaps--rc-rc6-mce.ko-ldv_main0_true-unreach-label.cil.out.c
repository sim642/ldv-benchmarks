/* Generated by CIL v. 1.5.1 */
/* print_CIL_Input is false */

#line 16 "include/asm-generic/int-ll64.h"
typedef unsigned char u8;
#line 22 "include/asm-generic/int-ll64.h"
typedef unsigned int u32;
#line 157 "include/linux/types.h"
typedef unsigned int gfp_t;
#line 177 "include/linux/types.h"
struct __anonstruct_atomic_t_6 {
   int counter ;
};
#line 177 "include/linux/types.h"
typedef struct __anonstruct_atomic_t_6 atomic_t;
#line 183 "include/linux/types.h"
struct list_head {
   struct list_head *next ;
   struct list_head *prev ;
};
#line 20 "./arch/x86/include/asm/spinlock_types.h"
struct qspinlock {
   atomic_t val ;
};
#line 33 "include/asm-generic/qspinlock_types.h"
typedef struct qspinlock arch_spinlock_t;
#line 33 "include/linux/bottom_half.h"
struct lockdep_map;
#line 55 "include/linux/debug_locks.h"
struct stack_trace {
   unsigned int nr_entries ;
   unsigned int max_entries ;
   unsigned long *entries ;
   int skip ;
};
#line 28 "include/linux/stacktrace.h"
struct lockdep_subclass_key {
   char __one_byte ;
};
#line 53 "include/linux/lockdep.h"
struct lock_class_key {
   struct lockdep_subclass_key subkeys[8U] ;
};
#line 59 "include/linux/lockdep.h"
struct lock_class {
   struct list_head hash_entry ;
   struct list_head lock_entry ;
   struct lockdep_subclass_key *key ;
   unsigned int subclass ;
   unsigned int dep_gen_id ;
   unsigned long usage_mask ;
   struct stack_trace usage_traces[13U] ;
   struct list_head locks_after ;
   struct list_head locks_before ;
   unsigned int version ;
   unsigned long ops ;
   char const   *name ;
   int name_version ;
   unsigned long contention_point[4U] ;
   unsigned long contending_point[4U] ;
};
#line 144 "include/linux/lockdep.h"
struct lockdep_map {
   struct lock_class_key *key ;
   struct lock_class *class_cache[2U] ;
   char const   *name ;
   int cpu ;
   unsigned long ip ;
};
#line 546 "include/linux/lockdep.h"
struct raw_spinlock {
   arch_spinlock_t raw_lock ;
   unsigned int magic ;
   unsigned int owner_cpu ;
   void *owner ;
   struct lockdep_map dep_map ;
};
#line 33 "include/linux/spinlock_types.h"
struct __anonstruct____missing_field_name_35 {
   u8 __padding[24U] ;
   struct lockdep_map dep_map ;
};
#line 33 "include/linux/spinlock_types.h"
union __anonunion____missing_field_name_34 {
   struct raw_spinlock rlock ;
   struct __anonstruct____missing_field_name_35 __annonCompField17 ;
};
#line 33 "include/linux/spinlock_types.h"
struct spinlock {
   union __anonunion____missing_field_name_34 __annonCompField18 ;
};
#line 76 "include/linux/spinlock_types.h"
typedef struct spinlock spinlock_t;
#line 32 "include/linux/mm_types.h"
struct kmem_cache;
#line 532 "include/linux/input.h"
enum rc_type {
    RC_TYPE_UNKNOWN = 0,
    RC_TYPE_OTHER = 1,
    RC_TYPE_LIRC = 2,
    RC_TYPE_RC5 = 3,
    RC_TYPE_RC5X = 4,
    RC_TYPE_RC5_SZ = 5,
    RC_TYPE_JVC = 6,
    RC_TYPE_SONY12 = 7,
    RC_TYPE_SONY15 = 8,
    RC_TYPE_SONY20 = 9,
    RC_TYPE_NEC = 10,
    RC_TYPE_SANYO = 11,
    RC_TYPE_MCE_KBD = 12,
    RC_TYPE_RC6_0 = 13,
    RC_TYPE_RC6_6A_20 = 14,
    RC_TYPE_RC6_6A_24 = 15,
    RC_TYPE_RC6_6A_32 = 16,
    RC_TYPE_RC6_MCE = 17,
    RC_TYPE_SHARP = 18,
    RC_TYPE_XMP = 19
} ;
#line 555 "include/linux/input.h"
struct rc_map_table {
   u32 scancode ;
   u32 keycode ;
};
#line 83 "include/media/rc-map.h"
struct rc_map {
   struct rc_map_table *scan ;
   unsigned int size ;
   unsigned int len ;
   unsigned int alloc ;
   enum rc_type rc_type ;
   char const   *name ;
   spinlock_t lock ;
};
#line 93 "include/media/rc-map.h"
struct rc_map_list {
   struct list_head list ;
   struct rc_map map ;
};
#line 29 "include/linux/types.h"
typedef _Bool bool;
#line 361 "./arch/x86/include/asm/pgtable_types.h"
struct page;
#line 26 "/home/cluser/ldv/inst/kernel-rules/verifier/rcv.h"
extern void *ldv_undef_ptr(void) ;
#line 293 "include/linux/slab.h"
void *ldv_kmem_cache_alloc_20(struct kmem_cache *ldv_func_arg1 , gfp_t flags ) ;
#line 18 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-rc6-mce.o.c.prepared"
void ldv_check_alloc_flags(gfp_t flags ) ;
#line 101 "include/media/rc-map.h"
extern int rc_map_register(struct rc_map_list * ) ;
#line 102
extern void rc_map_unregister(struct rc_map_list * ) ;
#line 19 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-rc6-mce.c"
static struct rc_map_table rc6_mce[63U]  = 
#line 19 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-rc6-mce.c"
  {      {2148467712U, 512U}, 
        {2148467713U, 513U}, 
        {2148467714U, 514U}, 
        {2148467715U, 515U}, 
        {2148467716U, 516U}, 
        {2148467717U, 517U}, 
        {2148467718U, 518U}, 
        {2148467719U, 519U}, 
        {2148467720U, 520U}, 
        {2148467721U, 521U}, 
        {2148467722U, 111U}, 
        {2148467723U, 28U}, 
        {2148467724U, 142U}, 
        {2148467725U, 226U}, 
        {2148467726U, 113U}, 
        {2148467727U, 358U}, 
        {2148467728U, 115U}, 
        {2148467729U, 114U}, 
        {2148467730U, 402U}, 
        {2148467731U, 403U}, 
        {2148467732U, 208U}, 
        {2148467733U, 168U}, 
        {2148467734U, 207U}, 
        {2148467735U, 167U}, 
        {2148467736U, 119U}, 
        {2148467737U, 128U}, 
        {2148467738U, 407U}, 
        {2148467739U, 412U}, 
        {2148467740U, 523U}, 
        {2148467741U, 522U}, 
        {2148467742U, 103U}, 
        {2148467743U, 108U}, 
        {2148467744U, 105U}, 
        {2148467745U, 106U}, 
        {2148467746U, 352U}, 
        {2148467747U, 174U}, 
        {2148467748U, 389U}, 
        {2148467749U, 386U}, 
        {2148467750U, 365U}, 
        {2148467751U, 372U}, 
        {2148467762U, 373U}, 
        {2148467763U, 425U}, 
        {2148467764U, 161U}, 
        {2148467770U, 225U}, 
        {2148467782U, 377U}, 
        {2148467783U, 392U}, 
        {2148467784U, 366U}, 
        {2148467785U, 212U}, 
        {2148467786U, 393U}, 
        {2148467788U, 368U}, 
        {2148467789U, 369U}, 
        {2148467790U, 210U}, 
        {2148467792U, 385U}, 
        {2148467802U, 370U}, 
        {2148467803U, 398U}, 
        {2148467804U, 399U}, 
        {2148467805U, 400U}, 
        {2148467806U, 401U}, 
        {2148467813U, 356U}, 
        {2148467822U, 164U}, 
        {2148467823U, 387U}, 
        {2148467840U, 224U}, 
        {2148467841U, 164U}};
#line 97 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-rc6-mce.c"
static struct rc_map_list rc6_mce_map  =    {{0, 0}, {(struct rc_map_table *)(& rc6_mce), 63U, 0U, 0U, 17, "rc-rc6-mce", {{{{{0}},
                                                                                   0U,
                                                                                   0U,
                                                                                   0,
                                                                                   {0,
                                                                                    {0,
                                                                                     0},
                                                                                    0,
                                                                                    0,
                                                                                    0UL}}}}}};
#line 106 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-rc6-mce.c"
static int init_rc_map_rc6_mce(void) 
{ 
  int tmp ;

  {
#line 108
  tmp = rc_map_register(& rc6_mce_map);
#line 108
  return (tmp);
}
}
#line 111 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-rc6-mce.c"
static void exit_rc_map_rc6_mce(void) 
{ 


  {
#line 113
  rc_map_unregister(& rc6_mce_map);
#line 114
  return;
}
}
#line 138
extern void ldv_check_final_state(void) ;
#line 147
extern void ldv_initialize(void) ;
#line 150
extern void ldv_handler_precall(void) ;
#line 153
extern int nondet_int(void) ;
#line 156 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-rc6-mce.c"
int LDV_IN_INTERRUPT  ;
#line 159 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-rc6-mce.c"
void main(void) 
{ 
  int tmp ;
  int tmp___0 ;
  int tmp___1 ;

  {
#line 171
  LDV_IN_INTERRUPT = 1;
#line 180
  ldv_initialize();
#line 186
  ldv_handler_precall();
#line 187
  tmp = init_rc_map_rc6_mce();
#line 187
  if (tmp != 0) {
#line 188
    goto ldv_final;
  } else {

  }
#line 190
  goto ldv_25641;
  ldv_25640: 
#line 193
  tmp___0 = nondet_int();
#line 193
  switch (tmp___0) {
  default: ;
#line 195
  goto ldv_25639;
  }
  ldv_25639: ;
  ldv_25641: 
#line 190
  tmp___1 = nondet_int();
#line 190
  if (tmp___1 != 0) {
#line 192
    goto ldv_25640;
  } else {

  }

#line 207
  ldv_handler_precall();
#line 208
  exit_rc_map_rc6_mce();
  ldv_final: 
#line 211
  ldv_check_final_state();
#line 214
  return;
}
}
#line 171 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/dscv/ri/43_2a/drivers/media/rc/keymaps/rc-rc6-mce.o.c.prepared"
void *ldv_kmem_cache_alloc_20(struct kmem_cache *ldv_func_arg1 , gfp_t flags ) 
{ 
  void *tmp ;

  {
#line 174
  ldv_check_alloc_flags(flags);
#line 175
  tmp = ldv_undef_ptr();
#line 175
  return (tmp);
}
}
#line 10 "/home/cluser/ldv/inst/kernel-rules/verifier/rcv.h"
__inline static void ldv_error(void) 
{ 


  {
  ERROR: ;
#line 12
  goto ERROR;
}
}
#line 25
extern int ldv_undef_int(void) ;
#line 7 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
bool ldv_is_err(void const   *ptr ) 
{ 


  {
#line 10
  return ((unsigned long )ptr > 2012UL);
}
}
#line 14 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
void *ldv_err_ptr(long error ) 
{ 


  {
#line 17
  return ((void *)(2012L - error));
}
}
#line 21 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
long ldv_ptr_err(void const   *ptr ) 
{ 


  {
#line 24
  return ((long )(2012UL - (unsigned long )ptr));
}
}
#line 28 "/home/cluser/ldv/inst/kernel-rules/kernel-model/ERR.inc"
bool ldv_is_err_or_null(void const   *ptr ) 
{ 
  bool tmp ;
  int tmp___0 ;

  {
#line 31
  if ((unsigned long )ptr == (unsigned long )((void const   *)0)) {
#line 31
    tmp___0 = 1;
  } else {
#line 31
    tmp = ldv_is_err(ptr);
#line 31
    if ((int )tmp) {
#line 31
      tmp___0 = 1;
    } else {
#line 31
      tmp___0 = 0;
    }
  }
#line 31
  return ((bool )tmp___0);
}
}
#line 20 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
int ldv_spin  =    0;
#line 24 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_check_alloc_flags(gfp_t flags ) 
{ 


  {
#line 27
  if (ldv_spin != 0 && (flags & 16U) != 0U) {
#line 27
    ldv_error();
  } else {

  }
#line 31
  return;
}
}
#line 30
extern struct page *ldv_some_page(void) ;
#line 33 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
struct page *ldv_check_alloc_flags_and_return_some_page(gfp_t flags ) 
{ 
  struct page *tmp ;

  {
#line 36
  if (ldv_spin != 0 && (flags & 16U) != 0U) {
#line 36
    ldv_error();
  } else {

  }
#line 38
  tmp = ldv_some_page();
#line 38
  return (tmp);
}
}
#line 42 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_check_alloc_nonatomic(void) 
{ 


  {
#line 45
  if (ldv_spin != 0) {
#line 45
    ldv_error();
  } else {

  }
#line 49
  return;
}
}
#line 49 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_spin_lock(void) 
{ 


  {
#line 52
  ldv_spin = 1;
#line 53
  return;
}
}
#line 56 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
void ldv_spin_unlock(void) 
{ 


  {
#line 59
  ldv_spin = 0;
#line 60
  return;
}
}
#line 63 "/home/cluser/mutilin/launch/work/current--X--drivers--X--defaultlinux-4.2-rc1.tar.xz--X--43_2a--X--cpachecker/linux-4.2-rc1.tar.xz/csd_deg_dscv/3828/dscv_tempdir/rule-instrumentor/43_2a/common-model/ldv_common_model.c"
int ldv_spin_trylock(void) 
{ 
  int is_lock ;

  {
#line 68
  is_lock = ldv_undef_int();
#line 70
  if (is_lock != 0) {
#line 73
    return (0);
  } else {
#line 78
    ldv_spin = 1;
#line 80
    return (1);
  }
}
}
